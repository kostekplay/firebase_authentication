////  SignUpViewController.swift
//  FirebaseAuthentication
//
//  Created on 12/06/2020.
//  Copyright © 2020 kostowski. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import Firebase

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var firstNameTfd: UITextField!
    @IBOutlet weak var lastNameTfd: UITextField!
    @IBOutlet weak var emailTfd: UITextField!
    @IBOutlet weak var passTfd: UITextField!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var errorLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpElements()
    }
    
    private func setUpElements(){
        
        errorLbl.alpha = 0
        
        Utilites.styleTextField(firstNameTfd)
        Utilites.styleTextField(lastNameTfd)
        Utilites.styleTextField(emailTfd)
        Utilites.styleTextField(passTfd)
        
        Utilites.styleFillButton(signUpBtn)
    }
    
    private func validaeFields() -> String? {
        if  firstNameTfd.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            lastNameTfd.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            emailTfd.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passTfd.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            return "Please fill in all fields."
        }
        
        let cleanPassword = passTfd.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if Utilites.isPasswordValid(cleanPassword) == false {
            return "Please make sure your password is at least 8 characters, contains a special character and a number."
        }
        
        return nil
    }
    
    @IBAction func singUpTapped(_ sender: UIButton) {
        let error = validaeFields()
        if error != nil {
            showError(error!)
        } else {
            
            let firstName = firstNameTfd.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let lastName = lastNameTfd.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let email = emailTfd.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let pass = passTfd.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            Auth.auth().createUser(withEmail: email, password: pass) { (res, err) in
                if err != nil {
                    self.showError("Error creating user.")
                } else {
                    print("User created")
                    let db = Firestore.firestore()
                    db.collection("users").addDocument(data: [  "first_name": firstName,
                                                                "last_name": lastName,
                                                                "uid": res!.user.uid])
                    {(error) in
                        if error != nil {
                            self.showError("Error saving user data.")
                        }
                    }
                    
                    self.transisionToHome()
                }
            }
        }
    }
    
    private func showError(_ message: String){
        errorLbl.text = message
        errorLbl.alpha = 1
    }
    
    func transisionToHome(){
        let homeViewController =
        storyboard?.instantiateViewController(identifier: Constans.Storyboard.homeVievController) as? HomeViewController
        view.window?.rootViewController = homeViewController
        view.window?.makeKeyAndVisible()
    }
}
