////  LoginViewController.swift
//  FirebaseAuthentication
//
//  Created on 12/06/2020.
//  Copyright © 2020 kostowski. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import Firebase

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTfd: UITextField!
    @IBOutlet weak var passTfd: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var errorLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpElements()
    }
    
    private func setUpElements(){
        
        errorLbl.alpha = 0
        
        Utilites.styleTextField(emailTfd)
        Utilites.styleTextField(passTfd)
        
        Utilites.styleFillButton(loginBtn)
    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
        let email = emailTfd.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let pass = passTfd.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        Auth.auth().signIn(withEmail: email, password: pass) { (res, err) in
            if err != nil {
                self.errorLbl.text = err!.localizedDescription
                self.errorLbl.alpha = 1
            } else {
                
                let homeViewController = self.storyboard?.instantiateViewController(identifier: Constans.Storyboard.homeVievController) as? HomeViewController
                
                self.view.window?.rootViewController = homeViewController
                self.view.window?.makeKeyAndVisible()
            }
        }
    }
}
