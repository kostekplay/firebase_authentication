////  ViewController.swift
//  FirebaseAuthentication
//
//  Created on 12/06/2020.
//  Copyright © 2020 kostowski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var singUpBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpElements()
    }

    private func setUpElements() {
        Utilites.styleFillButton(singUpBtn)
        Utilites.styleHollowButton(loginBtn)
    }
}

